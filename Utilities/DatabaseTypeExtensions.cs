﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PinefallStudios.Utilities
{
    public static class DatabaseTypeExtensions
    {
        public static SqlParameter AddWithNullableValue(this SqlParameterCollection collection, string parameterName, object value)
        {
            if (value == null)
                return collection.AddWithValue(parameterName, DBNull.Value);
            else
            {
                if (value is string && String.IsNullOrEmpty((string)value))
                    return collection.AddWithValue(parameterName, DBNull.Value);

                return collection.AddWithValue(parameterName, value);
            }
        }

        public static T GetEnum<T>(this SqlDataReader reader, string name) where T : struct
        {
            try
            {
                var value = reader.GetInt(name);
                T res = (T)Enum.ToObject(typeof(T), (int)value);
                if (!Enum.IsDefined(typeof(T), res))
                    return default(T);
                return res;
            }
            catch
            {
                return default(T);
            }
        }

        public static bool GetBoolean(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            return reader.GetBoolean(col);
        }

        public static DateTime GetDateTime(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            return reader.GetDateTime(col);
        }

        public static decimal GetDecimal(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            return reader.GetDecimal(col);
        }

        public static double GetDouble(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            return reader.GetDouble(col);
        }

        public static int GetInt(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            return reader.GetInt32(col);
        }

        public static string GetString(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            if (reader.IsDBNull(col)) return null;
            else return reader[col].ToString();
        }

        public static DateTime? GetNullableDateTime(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            return reader.IsDBNull(col) ?
                        (DateTime?)null :
                        (DateTime?)reader.GetDateTime(col);
        }

        public static decimal? GetNullableDecimal(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            return reader.IsDBNull(col) ?
                        (decimal?)null :
                        (decimal?)reader.GetDecimal(col);
        }

        public static double? GetNullableDouble(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            return reader.IsDBNull(col) ?
                        (double?)null :
                        (double?)reader.GetDouble(col);
        }

        public static T? GetNullableEnum<T>(this SqlDataReader reader, string name) where T : struct
        {
            try
            {
                var value = reader.GetNullableInt(name);
                if (value == null)
                    return (T?)null;
                T res = (T)Enum.ToObject(typeof(T), (int)value);
                if (!Enum.IsDefined(typeof(T), res)) return default(T);
                return res;
            }
            catch
            {
                return default(T);
            }
        }

        public static float? GetNullableFloat(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            if (reader.IsDBNull(col))
                return (float?)null;
            else
            {
                var value = reader.GetNullableDouble(name);
                return (float?)value;
            }
        }

        public static int? GetNullableInt(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);
            return reader.IsDBNull(col) ?
                        (int?)null :
                        (int?)reader.GetInt32(col);
        }

        public static string ToHardcoded(this object obj)
        {
            if (obj is null)
                return "null";

            if (obj is Enum)
                return ((int)obj).ToString();

            if (obj is int)
                return obj.ToString();

            if (obj is decimal)
                return obj.ToString();

            if (obj is string)
                return "'" + obj.ToString() + "'";

            if (obj is DateTime?)
            {
                var dt = (DateTime?)obj;
                if (dt.HasValue)
                    return "'" + ((DateTime)obj).ToString() + "'";
                else
                    return "null";
            }

            if (obj is DateTime)
                return "'" + obj.ToString() + "'";

            return String.Empty;

        }
    }
}
