﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PinefallStudios.Utilities
{
    public static class StringExtensions
    {
        public static bool ToBool(this string value)
        {
            if (String.IsNullOrEmpty(value))
                return false;
            else if (value.ToLower() == "false")
                return false;
            else if (value.ToLower() == "true")
                return true;
            else
                throw new ArgumentOutOfRangeException("Value is not a boolean: " + value.ToString());
        }

        public static DateTime ToDateTime(this string value)
        {
            DateTime myDate = DateTime.MinValue;
            if (DateTime.TryParse(value, out myDate))
                return myDate;
            else
                throw new ArgumentOutOfRangeException("Value is not a datetime: " + value.ToString());
        }

        public static decimal ToDecimal(this string value)
        {
            decimal myDecimal = 0;
            if (Decimal.TryParse(value, out myDecimal))
                return myDecimal;
            else
                throw new ArgumentOutOfRangeException("Value is not a decimal: " + value.ToString());
        }

        public static T ToEnum<T>(this string value) where T : struct
        {
            try
            {
                return Enum.Parse<T>(value);
            }
            catch
            {
                return default(T);
            }
        }

        public static int ToInt(this string value)
        {
            int myInt = -1;
            if (Int32.TryParse(value, out myInt))
                return myInt;
            else
                throw new ArgumentOutOfRangeException("Value is not a number: " + value.ToString());
        }

        public static DateTime? ToNullableDateTime(this string value)
        {
            if (String.IsNullOrEmpty(value))
                return null;
            else
            {
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParse(value, out dt))
                    return new DateTime?(dt);
                else
                    return null;
            }
        }

        public static string[] SplitCSV(this string input)
        {
            Regex csvSplit = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.Compiled);
            List<string> list = new List<string>();
            string curr = null;
            foreach (Match match in csvSplit.Matches(input))
            {
                curr = match.Value;
                if (0 == curr.Length)
                {
                    list.Add("");
                }

                list.Add(curr.TrimStart(','));
            }

            return list.ToArray();
        }
    }
}
